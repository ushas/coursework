from django.shortcuts import redirect, reverse

__ACCESS_TOKEN = None


def get_token():
    global __ACCESS_TOKEN
    return __ACCESS_TOKEN


def set_token(token):
    global __ACCESS_TOKEN
    __ACCESS_TOKEN = token


def unauthenticated_user(view_func):
    """
    Checked if user is authorized
    """
    def wrapper_func(request, *args, **kwargs):
        if get_token():
            return view_func(request, *args, **kwargs)
        else:
            return redirect(reverse('login'))
    return wrapper_func
